// const baseURL = "​​https://9otavzx2e3.execute-api.us-west-2.amazonaws.com/production/v4/"
// const newURL = `https://api.igdb.com/v4/${url}`;
// "x-api-key": 'kisjj1ibx9',
const headers = {
    'Authorization': 'Bearer mno4nc48lnud2b04h7qtw3x5njn9ma',
    'Client-ID': 'ievt8o1hvdmi7umjcu9tow0f85gdet',
}
 
 export async function searchIGDB(url: string, name:string) {
    if (!url) return;
    const newURL = `https://api.igdb.com/v4/${url}`;
    const bodySearch = name ? `search ${name}; limit 10;` : `fields *; limit 10;`

    // TODO: API
    // const newURL = `https://m6ylh4al76.execute-api.us-west-2.amazonaws.com/production/v4/${url}`;

    const resp = await fetch(newURL, {
        headers: headers,
        method: 'POST',
        body: bodySearch,
    });

    return resp.json();
}

export async function getIGDB(url:string) {
    if (!url) return;
    const newURL = `https://api.igdb.com/v4/${url}`;

    // TODO: API
    // const newURL = `https://m6ylh4al76.execute-api.us-west-2.amazonaws.com/production/v4/${url}`

    const resp = await fetch(newURL, {
        headers: headers,
        method: 'POST',
        body: "fields *; limit 10;",
    })

    return resp.json();
}
import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: ({
        'black': '#0a0a0a',
        'blue': '#1fb6ff',
        'blue-dark': '#1e40af',
        'purple': '#7e5bef',
        'pink': '#ec4899',
        'orange-light': '#ff7849',
        'orange-dark': '#eb5c28',
        'green': '#84cc16',
        'green-dark': '#65a30d',
        'gray-light': '#e5e5e5',
        'gray': '#b5b3b3',
        'red-light': '#ef4444',
        'red': '#b91c1c',
        'red-dark': '#991b1b',
        'yellow': '#eab308',
        'yellow-dark': '#ca8a04',
        'white': '#fafafa',
        'white-dark': '#e7e7e7',
      }),
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
export default config

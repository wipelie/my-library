const { env } = require('process')

/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        NEXT_PUBLIC_SUPABASE_URL: process.env.API_URL,
        NEXT_PUBLIC_SUPABASE_ANON_KEY: process.env.ANON_KEY_PUBLIC,
    },
    images: {
        remotePatterns: [{
            protocol: 'https',
            hostname: 'rtxzuhcbjmtciefnnupk.supabase.co',
            port: '',
        }],
    },
}

module.exports = nextConfig


"use client"

import { useQuery } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsoleByUUIDQuery } from "@/queries/get-consoles-by-uuid";

function useConsoleByUUID(consoleUUID: string) {
    const client = useSupabaseClient();
    const queryKey = ['console', consoleUUID];
   
    const queryFn = async () => {
      return getConsoleByUUIDQuery(client, consoleUUID).then(
        (result) => result.data
      );
    };
   
    return useQuery({ 
      queryKey,
      queryFn,
      staleTime: 5000 
    });
  }
   
  export default useConsoleByUUID;
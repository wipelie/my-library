import { useQuery, useQueryClient } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsolesQuery } from "../../queries/get-consoles";

function usePrefetchConsoles(): void {
    const client = useSupabaseClient();
    const QueryClient = useQueryClient();

    const queryFn = async () => {
        return getConsolesQuery(client).then(
            (result) => result.data
        );
    };

    QueryClient.prefetchQuery({
        queryKey: ["consoles"],
        queryFn
    })

  }
   
  export default usePrefetchConsoles;
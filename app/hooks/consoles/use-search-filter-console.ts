
"use client"

import { useQuery } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsoleBySearchAndFilterQuery } from "@/queries/get-consoles-by-search-and-filter";

interface Filters {
  name: string,
  value: string,
}


function useSearchAndFilterConsole(textSearch: string, filters: Filters) {
    const client = useSupabaseClient();
    const { name, value } = filters;

    const queryKey = ["console-search-filter", value];

    const queryFn = async () => {
      return getConsoleBySearchAndFilterQuery(client, textSearch, name, value).then(
        (result) => result.data
      );
    };
   
    return useQuery({ 
      queryKey,
      queryFn,
      staleTime: 5000,
      enabled: Boolean(name)
    });
  }
   
  export default useSearchAndFilterConsole;
import useSupabaseClient from "../use-supabase-client";
import { addConsoleQuery } from "../../queries/add-console";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useState } from "react";
import { useRouter } from 'next/navigation'
import { uploadFileQuery } from "@/queries/upload-file";


function useAddConsoleMutation() {
    const [file, setFile] = useState({
      content: null,
      path: null
    })
    const client = useSupabaseClient();
    const queryClient = useQueryClient();
    const router = useRouter();

    const mutationFn = async (data) => {
      if (data?.file) {
        setFile({
          content: data.file,
          path: data.fileName
        });
      }

      return addConsoleQuery(client, { ...data, img: file.path})
    }

    const onError = (error) => {
        toast.error(error.toString());
    }

    const onSuccess = () => {
      if (file) {
        toast.info("Form accepted, uploading your image...");
        uploadFileQuery(client, file.content, file.path, "consoles").then(() => {
          toast.success("Successfully added !");
        }, (error) => {
            console.log(error)
        });
      } else {
        toast.success("Successfully created !");
      }

      queryClient.invalidateQueries({ queryKey: ['consoles'] });
      router.push('/consoles');
    }
  
    return useMutation({
        mutationKey: ["new"],
        mutationFn,
        onError,
        onSuccess,
    });
  }
       
export default useAddConsoleMutation
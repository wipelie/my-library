import useSupabaseClient from "../use-supabase-client";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useRouter } from 'next/navigation'
import { updateConsoleQuery } from "@/queries/update-console";

interface Props {
  slug: string,
  description: string,
}

function useUpdateConsoleMutation() {
    const client = useSupabaseClient();
    const queryClient = useQueryClient();
    const router = useRouter();
   
    const mutationFn = async ({ slug, description }: Props) => {
      return updateConsoleQuery(client, slug, description);
    }

    const onError = (error: any) => {
        toast.error(error.toString());
    }

    const onSuccess = () => {
    toast.success("Successfully updated !");
      queryClient.invalidateQueries({ queryKey: ['consoles'] })
      router.push('/consoles');
    }
  
    return useMutation({
        mutationKey: ["remove"],
        mutationFn,
        onError,
        onSuccess,
    });
  }
       
export default useUpdateConsoleMutation
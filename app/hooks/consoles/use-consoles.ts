import { useQuery } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsolesQuery } from "../../queries/get-consoles";

function useConsoles() {
    const client = useSupabaseClient();

    const queryFn = async () => {
      return getConsolesQuery(client).then(
        (result) => result.data
      );
    };

    return useQuery({ 
      queryKey: ["consoles"],
      queryFn,
      staleTime: 5000,
    });
  }
   
  export default useConsoles;
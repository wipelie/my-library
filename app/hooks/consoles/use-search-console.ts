
"use client"

import { useQuery } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsoleBySearchQuery } from "@/queries/get-consoles-by-search";

function useSearchConsole(name: string) {
    const client = useSupabaseClient();
    const queryKey = ["console-search", name];

    const queryFn = async () => {
      return getConsoleBySearchQuery(client, name).then(
        (result) => result.data
      );
    };
   
    return useQuery({ 
      queryKey,
      queryFn,
      staleTime: 5000,
      enabled: Boolean(name)
    });
  }
   
  export default useSearchConsole;
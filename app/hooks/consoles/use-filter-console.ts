
"use client"

import { useQuery } from "@tanstack/react-query";

import useSupabaseClient from "../use-supabase-client";
import { getConsoleByFilterQuery } from "@/queries/get-consoles-by-filter";

function useFilterConsole(filters: object, hasFilters: boolean) {
    const client = useSupabaseClient();
    const { name, value } = filters;
    const queryKey = ["console-search", value];

    const queryFn = async () => {
      return getConsoleByFilterQuery(client, name, value).then(
        (result) => result.data
      );
    };
   
    return useQuery({ 
      queryKey,
      queryFn,
      staleTime: 5000,
      enabled: hasFilters
    });
  }
   
  export default useFilterConsole;
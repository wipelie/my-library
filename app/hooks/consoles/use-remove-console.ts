import useSupabaseClient from "../use-supabase-client";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { removeConsoleQuery } from "@/queries/remove-console";
import { useRouter } from 'next/navigation'
import { useState } from "react";
import { deleteFileQuery } from "@/queries/delete-file";

function useRemoveConsoleMutation() {
    const [imgName, setImgName] = useState(null)

    const client = useSupabaseClient();
    const queryClient = useQueryClient();
    const router = useRouter();
   
    const mutationFn = async ({ uuid, img }) => {
      setImgName(img)
      return removeConsoleQuery(client, uuid);
    }

    const onError = (error) => {
        toast.error(error.toString());
    }

    const onSuccess = () => {
      if (imgName) {
        toast.info("Console deleted, removing your image...");
        deleteFileQuery(client, imgName, "consoles").then(() => {
          toast.success("Successfully removed !");
        }, (error) => {
            console.log(error)
        });
      } else {
        toast.success("Successfully removed !");
      }

      queryClient.invalidateQueries({ queryKey: ['consoles'] })
      router.push('/consoles');
    }
  
    return useMutation({
        mutationKey: ["remove"],
        mutationFn,
        onError,
        onSuccess,
    });
  }
       
export default useRemoveConsoleMutation
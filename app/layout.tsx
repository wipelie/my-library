"use client"

import { ReactNode } from 'react'
import { ReactQueryClientProvider } from './_components/ReactQueryClientProvider'

import Body from './_components/Body'

import './_styles/global.css'

// export const metadata: Metadata = {
//   title: 'Games Library',
//   description: 'Your own library !',
// }

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <html lang="en">
      <ReactQueryClientProvider>
          <head>
            {/* <link href="https://unpkg.com/nes.css@latest/css/nes.min.css" rel="stylesheet" /> */}
          </head>
          <Body>
            {children}
          </Body> 
      </ReactQueryClientProvider>
    </html>
  )
}

export default Layout

'use client'

import GamesList from "./gamesList";

export default function GamesDashboard({ data }) {
  return (
    <div className="consoles-dashboard-container
    mb-32 text-center lg:w-full lg:mb-0 lg:text-center">
      <div className={`nes-container with-title is-centered font-mono m-10`}>
          Games Dashboard
      </div>
      {/* <AddButton type={"game"} /> */}
      {/* <GamesList data={data} /> */}
    </div>
  )
}

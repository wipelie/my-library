import GamesListDetails from "./GamesListDetails"

export default function ConsolesList({ data }) {
  return (
    <div className="m-10 grid grid-cols-3 gap-3
    consoles-list-container">
      {data?.map(game => 
          <GamesListDetails key={`key-${game.slug}`} game={game} />
      )}      
    </div>
  )
}

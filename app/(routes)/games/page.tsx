import GamesDashboard from "./gamesDashboard";

export default async function Consoles() {    
    return (
        <div className="page-games-container min-h-screen flex-col text-center lg:p-20"> 
            <GamesDashboard />
        </div>
    )
}

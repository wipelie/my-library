import Link from "next/link"

export default function Lists() {
    const listsNames = ['Consoles', 'Games', 'Mangas', 'BDs']
    return (
      <div className="flex min-h-fit flex-col items-center justify-center p-24">
        <div id="containers" className="item">
            {listsNames.map(name => 
                <div key={`list-${name}`} className={`m-5 nes-container`}>
                    <Link href={`${name.toLowerCase()}`}>{name}</Link>
                </div>
            )}
        </div>
      </div>
    )
  }
'use client'

import usePrefetchConsoles from "@/hooks/consoles/use-prefetch-consoles";
import Lists from "./lists";

export default function Dashboard() {
  usePrefetchConsoles();

  return (
    <div className="flex min-h-screen flex-col text-center items-center justify-between p-24">
      <div className="mb-32 text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:text-center">
        <div className={`nes-container with-title is-centered font-mono`}>
            Welcome to your Dashboard !
        </div>
        <Lists />
      </div>
    </div>
  )
}

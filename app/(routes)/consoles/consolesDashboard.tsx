'use client'

import { ChangeEventHandler } from "react"

import ConsolesList from "./consolesList"
import DashboardActions from "@/_components/_actions/DashboardActions"
import { Loading } from "@/_components/Loading"
interface Props {
  data: Array<object>,
  onSearch: ChangeEventHandler,
  onBrandChange: ChangeEventHandler,
  loading: boolean
}

export default function ConsolesDashboard({ data, onSearch, onBrandChange, loading }: Props) {
  return (
    <div className="consoles-dashboard-container
    mb-32 text-center lg:w-full lg:mb-0 lg:text-center min-h-screen flex flex-col"
    >
      <DashboardActions onSearch={onSearch} onBrandChange={onBrandChange} />
      <div className={`nes-container`}>
          <h1 className="with-title is-centered font-mono m-10">Consoles Dashboard {data?.length && `(${data.length})`}</h1>
          { loading ?
            <Loading loading={loading} />
          :
            <>
              { !data?.length ? 
                <p>No Data found.</p> 
              :
                <ConsolesList data={data}/>
              }
            </>
          }

      </div>
    </div>
  )
}

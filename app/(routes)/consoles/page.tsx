"use client"

import useConsoles from "@/hooks/consoles/use-consoles";
import ConsolesDashboard from "./consolesDashboard";
import { showToast } from "@/_utils/show-message";
import { Loading } from "@/_components/Loading";
import useSearchConsole from "@/hooks/consoles/use-search-console";
import { SetStateAction, useState } from "react";
import useFilterConsole from "@/hooks/consoles/use-filter-console";
import useSearchAndFilterConsole from "@/hooks/consoles/use-search-filter-console";

export default function Consoles() {    
    const [searchTerm, setSearchTerm] = useState("");
    const [filters, setFilters] = useState([]);
    const [hasFilters, setHasFilters] = useState(false)

    const { 
        data: basicData, 
        isError,
        isLoading
      } = useConsoles();

    const { 
        data: searchData, 
        isError: isErrorSearch,
        isLoading: isLoadingSearch
    } = useSearchConsole(searchTerm);

    // TODO: check supabase API

    // const {
    //     data: filterAndSearchData, 
    //     isError: isErrorFilterandSearch,
    //     isLoading:isLoadingFilterandSearch
    // } = useSearchAndFilterConsole(searchTerm, "consoles", filters)

    const { 
        data: filterData, 
        isError: isErrorFilterData,
        isLoading:isLoadingFilterData
    } = useFilterConsole(filters, hasFilters);
    
    const onSearch = (event: { target: { value: SetStateAction<string>; }; }) => {
        setSearchTerm(event.target?.value);
    }

    const onBrandChange = (filter: SetStateAction<never[]>) => {
        setFilters(filter);
        setHasFilters(!!filter.value);
    }

    if (isError) {
        showToast();
        return;
    }
    
    const showData = searchTerm ? searchData : basicData;
    const data = hasFilters ? filterData : showData;

    const hasLoading = isLoading || isLoadingFilterData || isLoadingSearch

    return (
        <div className="page-consoles-container flex-col text-center lg:p-20 min-h-screen"> 
            <ConsolesDashboard data={data} onSearch={onSearch} onBrandChange={onBrandChange} loading={hasLoading} />
        </div>
    )
}

import Card from "@/_components/_cards/Card"
import { GetImage } from "@/_utils/get-images"

interface PropsURL {
  console: {
    name: string,
    slug: string,
    details?: string,
    img?: string,
    date?: string,
    uuid: string;
  }
}

export default function ConsolesListDetails({ console }: PropsURL) {
  const urlImg = GetImage(console.img)

  return (
    <div className="consoles-list-details-container
      h-full
    ">
      <Card urlImg={urlImg} goTo={`consoles/${console.uuid}`} data={console} />
    </div>
  )
}

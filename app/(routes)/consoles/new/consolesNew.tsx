import DetailsActions from "@/_components/_actions/DetailsActions"
import AddConsoleForm from "@/_components/_forms/AddConsoleForm"

export default function ConsolesNew({ addMutation }) {
    return (
        <div className="consoles-new-container min-h-screen min-w-20">
            <DetailsActions backTo={"/consoles"} canRemove={false} canModify={false} />
            <h2>Not in your database ? Add it !</h2>
            <AddConsoleForm addMutation={addMutation}/>
        </div>
    )
}

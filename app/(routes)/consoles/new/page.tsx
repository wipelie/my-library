'use client'

import ConsolesNew from "./consolesNew";
import useAddConsoleMutation from "@/hooks/consoles/use-add-console";

export interface Console {
    name: string,
    slug: string,
    details?: string,
    img?: string,
    date?: string
}

export default function NewConsole() {
    // const searchInIGDB = getIGDB('games').then(resp => {return resp})
    // console.log(searchInIGDB, 'sEARCH')

    const addMutation = useAddConsoleMutation();

    return (
        <div className="page-consoles-container flex-col text-center p-10 lg:p-15 min-h-screen"> 
            <ConsolesNew addMutation={addMutation} />
        </div>
    )
}
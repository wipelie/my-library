import DetailsActions from "@/_components/_actions/DetailsActions";
import UpdateConsoleForm from "@/_components/_forms/UpdateConsoleForm";

interface Props {
    data: object,
    uuid: string,
    updateMutation: any,
}  

export default function ConsoleUpdate({ data, uuid, updateMutation }: Props) {
    return (
        <div className="consoles-new-container min-h-screen min-w-20">
            <DetailsActions backTo={`/consoles/${uuid}`} canRemove={false} canModify={false} />
            <h2>Modify your console</h2>
            { data && <UpdateConsoleForm data={data} uuid={uuid} updateMutation={updateMutation}/> }
        </div>
    )
}

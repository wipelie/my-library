'use client'

import useUpdateConsoleMutation from "@/hooks/consoles/use-update-console";
import ConsoleUpdate from "./consoleUpdate";
import useConsoleByUUID from "@/hooks/consoles/use-console-by-uuid";

export interface Console {
    name: string,
    slug: string,
    details?: string,
    img?: string,
    date?: string
}

interface PropsURL {
    params: paramsURL
}

interface paramsURL {
    uuid: string,
    type: string,
}

export default function UpdateConsoleForm({ params } : PropsURL ) {
    const { uuid } = params;

    const { 
        data,
      } = useConsoleByUUID(uuid)

    const updateMutation = useUpdateConsoleMutation();

    return (
        <div className="page-consoles-container flex-col text-center p-10 lg:p-15 min-h-screen"> 
            <ConsoleUpdate updateMutation={updateMutation} data={data} uuid={uuid} />
        </div>
    )
}

import DetailsActions from "@/_components/_actions/DetailsActions";
import { GetImage } from "@/_utils/get-images";
import Image from "next/image"
import { MouseEventHandler } from "react";

interface PropsConsolesDetails {
  console: Console,
  onRemove: MouseEventHandler,
}
interface Console {
  name: string,
  uuid: string,
  description?: string,
  date?: string,
  img?: string,
}

export default function ConsolesDetails({ console, onRemove }: PropsConsolesDetails) {
  if (!console) return;

  const urlImg = GetImage(console.img)

  return (
    <div className="consoles-details-container min-h-full min-w-20">
        <DetailsActions type={"consoles"} uuid={console?.uuid} backTo={"/consoles"} onRemove={onRemove} />
        <div className="p-10 flex flex-col items-center">
          <Image
            src={urlImg}
            width={350}
            height={350}
            alt={console.name}
            className="p-10"
          />
          <h2 className="text-2xl p-5">{console.name}</h2>
        </div>
        <div className="flex p-5">
          <p>{console.description}</p>
        </div>

    </div>
  )
}

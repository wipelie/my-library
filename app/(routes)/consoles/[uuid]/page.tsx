"use client"

import useConsoleBySlugQuery from "@/hooks/consoles/use-console-by-uuid";
import ConsolesDetails from "./consolesDetails";
import { Loading } from "@/_components/Loading";
import { showToast } from "@/_utils/show-message";
import useRemoveConsoleMutation from "@/hooks/consoles/use-remove-console";
import { useModal } from "@/providers/modalProvider";
import { useEffect } from "react";

export interface Console {
    name: string,
    slug: string,
    details?: string,
    date?: string
}

interface PropsURL {
    params: paramsURL
}

interface paramsURL {
    uuid: string
}

export default function Consoles({ params }: PropsURL) {
    const { uuid } = params
    const dialog = useModal()
    const removeMutation = useRemoveConsoleMutation();

    const { 
        data, 
        isError,
        isLoading
      } = useConsoleBySlugQuery(uuid);

    useEffect(() => {
        if (dialog.confirm) {
            removeMutation.reset();
            removeMutation.mutate({ uuid: uuid, img: data.img });
        }
        
        return () => {
            dialog.setConfirm(false);
            dialog.onClose()
        }
    }, [dialog.confirm])
      

    const onRemove = () => {
        dialog.onOpen();

    }

    if (isError) {
        showToast("Error, try again");
        return;
    }

    return (
        <div className="page-consoles-container flex-col text-center p-10 lg:p-15 min-h-screen">
            <Loading loading={isLoading} />
            <ConsolesDetails console={data} onRemove={onRemove} />
        </div>
    )
}

import ConsolesListDetails from "./consolesListDetails"

export default function ConsolesList({ data }) {
  return (
    <div className="grid lg:grid-cols-3 sm::grid-cols-1 gap-3
    consoles-list-container sm:p-5 lg:p-20 md-10">
      {data?.map(console => 
          <ConsolesListDetails key={`key-${console.slug}`} console={console} />
      )}      
    </div>
  )
}

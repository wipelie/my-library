"use client"
import { useAuth } from '@/providers/authProvider';

export default function Profile() {
  const { user, token, logOut }= useAuth();

  if (!token || !user) return null;

  return (
    <div className="min-h-screen flex items-center justify-center flex-col">
      <h1>Hello, {user.username || 'user'}!</h1>
      <p>Your mail: {user.email}</p>
      <button className='btn m-5' onClick={logOut}>Logout</button>
    </div>
  )
}
'use client'

import { useRouter } from 'next/navigation'

import { createClient } from '@/_utils/supabase/component'
import { showToast } from '@/_utils/show-message'
import { useState } from 'react'
import { useAuth } from '@/providers/authProvider'

export default function LoginPage() {
  const supabase = createClient()
  const auth = useAuth()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  async function logIn() {
    const { data, error } = await supabase.auth.signInWithPassword({ email, password })

    if (error) {
      showToast(error.toString())
    } else {
      auth.loginAction(data)
    }
  }

  async function signUp() {
    const { error } = await supabase.auth.signUp({ email, password })
    if (error) {
      showToast(error.toString())
    } else {
      showToast("Ok, check your emails !", "success")
    }
  }

  return (
    <form className="login-container flex flex-col items-center justify-center min-h-screen">
      <label htmlFor="email">Email:</label>
      <input
        id="email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        autoComplete='username'
      />
      <label htmlFor="password">Password:</label>
      <input
        id="password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        autoComplete='current-password'
      />
      <button className="btn login-btn" type="button" onClick={logIn}>
        Log in
      </button>
      <button className="btn login-btn" type="button" onClick={signUp}>
        Sign up
      </button>
    </form>
  )
}
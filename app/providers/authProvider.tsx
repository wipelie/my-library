import { showToast } from "@/_utils/show-message";
import useSupabaseClient from "@/hooks/use-supabase-client";
import { getProfile } from "@/queries/get-profile";
import { usePathname, useRouter } from "next/navigation";
import { useState, ReactNode, createContext, useContext, useEffect } from "react";

interface LayoutProps {
    children: ReactNode;
}

const AuthContext = createContext({});

const AuthProvider = ({ children }: LayoutProps) => {
    const client = useSupabaseClient();
    const router = useRouter();

    const [user, setUser] = useState(null);
    const [token, setToken] = useState(false);

    useEffect(() => {
        const getUser = async () => {

            const user = await getProfile(client);

            console.log('hhh')
            if (user) {
                setUser(user);
                setToken(!!user);
            } else {
                router.push("/login");
            }
        };

        getUser();

        return () => {
            setUser(null);
        }
    }, [])

    const loginAction = (data) => {
        if (data) {
            const { user } = data;

            setUser(user.id);
            if (data.session) onSetSession({ session : data.session });
            router.push("/dashboard");
            return;
        }
    };

    const logOut = async () => {
        const { error } = await client.auth.signOut();

        if (error) {
            console.error(error);
        } else {
            setUser(null);
            onSetSession({ action : false});
            router.push("/login");
        }
    };

    const onSetSession = ({ session = {}, action = true }) => {
        if (action) {
            setToken(!!session.access_token);
            localStorage.setItem("site", session.access_token);
        } else {
            setToken(false);
            setUser(null);
            localStorage.removeItem("site");
            router.push('/login');
        }
    }

    return (
        <AuthContext.Provider value={{ token, user, loginAction, logOut }}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;

export const useAuth = () => {
    return useContext(AuthContext);
};
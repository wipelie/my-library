import ConfirmDialog from "@/_components/_dialogs/ConfirmDialog";
import { useState, ReactNode, createContext, useContext } from "react";
import {AlertDialog, AlertDialogBody, AlertDialogContent, AlertDialogFooter, AlertDialogHeader, AlertDialogOverlay, Button, useDisclosure } from "@chakra-ui/react"
import { useRef } from "react"

interface LayoutProps {
    children: ReactNode;
}

const ModalContext = createContext({});

const ModalProvider = ({ children }: LayoutProps) => {
    const [confirm, setConfirm] = useState(false)
    const { isOpen, onOpen, onClose } = useDisclosure()
    const cancelRef = useRef()

    return (
        <ModalContext.Provider value={{onOpen, confirm, setConfirm, onClose}}>
            {children}
            <AlertDialog
                isOpen={isOpen}
                leastDestructiveRef={cancelRef}
                onClose={onClose}
                >
                <AlertDialogOverlay>
                    <AlertDialogContent>
                    <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                        Confirm Delete
                    </AlertDialogHeader>
        
                    <AlertDialogBody>
                        Are you sure? You can&#39;t undo this action afterwards.
                    </AlertDialogBody>
        
                    <AlertDialogFooter>
                        <Button ref={cancelRef} onClick={onClose}>
                        Cancel
                        </Button>
                        <Button colorScheme='red' onClick={() => setConfirm(true)} ml={3}>
                        Delete
                        </Button>
                    </AlertDialogFooter>
                    </AlertDialogContent>
                </AlertDialogOverlay>
            </AlertDialog>
        </ModalContext.Provider>
    );
};

export default ModalProvider;

export const useModal = () => {
    return useContext(ModalContext);
};
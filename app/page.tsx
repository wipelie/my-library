import Link from 'next/link'

import { NextPageWithLayout } from '@/_types/page';
import Layout from './_components/Layout';

const Home: NextPageWithLayout = () => {
  return (
    <div className="flex min-h-screen flex-col items-center justify-center p-24">
      <div className="z-10 max-w-5xl w-full items-center justify-center flex-col-reverse font-mono text-sm lg:flex">
        <Link href="/dashboard">Let's go !</Link>
      </div>
    </div>
  );
};

Home.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default Home;
import { TypedSupabaseClient } from "../_utils/types";

export function getConsolesQuery(client: TypedSupabaseClient) {
    // return client.rpc('all_consoles');

      return client
          .from('consoles')
          .select('name, slug, img, uuid')
          .order('slug', { ascending: true })
          .throwOnError();
  }
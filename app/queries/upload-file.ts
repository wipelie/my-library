import { showToast } from "@/_utils/show-message";
import { TypedSupabaseClient } from "../_utils/types";

export async function uploadFileQuery(client: TypedSupabaseClient, file: File, filePath: string, type: string) {
    try {
        await client.storage.from(type).upload(filePath, file, {
            cacheControl: "3600",
            upsert: false,
        });
    } catch (error: any) {
        showToast(error)
    }
  }
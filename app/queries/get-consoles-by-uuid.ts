import { TypedSupabaseClient } from '@/_utils/types'

export function getConsoleByUUIDQuery(client: TypedSupabaseClient, uuid: string) {
  return client
    .from('consoles')
    .select(`*`)
    .eq('uuid', uuid)
    .throwOnError()
    .single()
}
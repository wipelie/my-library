import { showToast } from "@/_utils/show-message";
import { TypedSupabaseClient } from "../_utils/types";

export async function replaceFileQuery(client: TypedSupabaseClient, file: File, filePath: string, type: string) {
    try {
        await client.storage.from(type).update(
            filePath,
            file,
        );
    } catch (error: any) {
        showToast(error)
    }
  }
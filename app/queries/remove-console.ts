import { TypedSupabaseClient } from "../_utils/types";

export function removeConsoleQuery(client: TypedSupabaseClient, uuid: string) {
    return client
        .from('consoles')
        .delete()
        .eq('uuid', uuid)
        .throwOnError();
  }
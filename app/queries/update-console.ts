import { TypedSupabaseClient } from "../_utils/types";

// Modifiable columns:
//  - Description
//  - Date
// ex: { "description": "I love Mario"}

// TODO : Image
// Name can be update ?

export function updateConsoleQuery(client: TypedSupabaseClient, slug: string, description: string) {
    return client
        .from('consoles')
        .update({ "description": description })
        .eq('slug', slug)
        .throwOnError();
  }
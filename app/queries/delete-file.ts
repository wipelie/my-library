import { showToast } from "@/_utils/show-message";
import { TypedSupabaseClient } from "../_utils/types";

export async function deleteFileQuery(client: TypedSupabaseClient, imgName: string, type: string) {
    const { data, error } = await client
        .storage
        .from(type)
        .remove([imgName])
    if (error) {
        showToast(error.toString())
    }

    return data
  }
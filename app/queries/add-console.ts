import { TypedSupabaseClient } from "../_utils/types";

interface newConsole {
    name: string;
    description: string;
    fileName?: string,
    slug: string;
}

export function addConsoleQuery(client: TypedSupabaseClient, data: newConsole) {
    const { name, description, fileName, slug } = data

    return client
        .from('consoles')
        .insert([
            { name, description, img: fileName, slug }
        ])
        .select()
        .throwOnError();
  }
import { showToast } from "@/_utils/show-message";
import { TypedSupabaseClient } from "../_utils/types";

export async function getProfile(client: TypedSupabaseClient) {
    const { data: { user } } = await client.auth.getUser()

    let { data , error, status } = await client
        .from('profiles')
        .select(`*`)
        .eq('id', user.id)
        .single();

    if (error && status !== 406) {
        showToast(error.toString());
        return;
    }

    return {
        ...data,
        email: user.email
    }

  }
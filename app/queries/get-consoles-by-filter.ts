import { TypedSupabaseClient } from '@/_utils/types'

export function getConsoleByFilterQuery(client: TypedSupabaseClient, name: string, value: string) {
  return client
    .from('consoles')
    .select()
    .eq(name, value)
    .throwOnError()
}
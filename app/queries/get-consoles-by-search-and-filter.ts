import { TypedSupabaseClient } from '@/_utils/types'

export function getConsoleBySearchAndFilterQuery(client: TypedSupabaseClient, textSearch: string, name: string, value: string) {
  console.log(textSearch, name, value)
  return client
    .from('consoles')
    .select()
    .textSearch('name', textSearch)
    .eq(name, value)
    .throwOnError()
}
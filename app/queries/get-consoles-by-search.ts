import { TypedSupabaseClient } from '@/_utils/types'

export function getConsoleBySearchQuery(client: TypedSupabaseClient, name: string) {
  return client
    .from('consoles')
    .select()
    .textSearch('name', name)
    .throwOnError()

}
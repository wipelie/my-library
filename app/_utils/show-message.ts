import { toast } from "react-toastify"

export function showToast (data?: string, type = "error") {
    switch (type) {
        case "error":
            toast.error(data || "Something is broken, oops!")
            break;
        case "success":
            toast.success(data)
        default:
            break;
    }
}
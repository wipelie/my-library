import useSupabaseClient from "../hooks/use-supabase-client";

export function GetImageFromBucket(type: string, img: string) {
  const client = useSupabaseClient();

  const { data } = client.storage.from(type).getPublicUrl(img)

  return (data.publicUrl)
}

export function GetImage(img?: string) {
  const url = img ? GetImageFromBucket("consoles", img) : "/not-found.png";

  return url;
}
import { showToast } from "@/_utils/show-message";

export function hasName(name:string) {
    if (!name) {
        showToast("Required: name");
    }

    return !!name;
}
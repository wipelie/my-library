export const colorVariants: { [unit: string]: string } = {
    black: "text-black/90",
    blue: "text-blue/90",
    bluedark: "text-blue-dark/90",
    green: "text-green/90",
    greendark: "text-green-dark/90",
    purple: "text-purple/90",
    redlight: "text-red-light/90",
    red: "text-red/90",
    reddark: "text-red-dark/90",
    yellow: "text-yellow/90",
    yellowdark: "text-yellow-dark/90",
};

export const brands = [
    {key: "", value: "All brands" },
    {key: "atari", value: "Atari" },
    {key: "mattel", value: "Mattel" },
    {key: "markint", value: "Markint" },
    {key: "microsoft", value: "Microsoft" },
    {key: "nintendo", value: "Nintendo" },
    {key: "philips", value: "Philips" },
    {key: "sega", value: "Sega" },
    {key: "sony", value: "Sony" },
    {key: "soundic", value: "Soundic" },
    {key: "watara", value: "Watara" },
];
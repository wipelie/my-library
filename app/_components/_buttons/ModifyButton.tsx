'use client'

import ModifyIcon from "@/_icons/modify"
import Link from "next/link"

interface Props {
    uuid: string,
    type: string,
}

export default function ModifyButton({ type, uuid }: Props) {
    return (
        <Link
            href={`/${type}/${uuid}/update`}
            className="btn modify-btn flex items-center rounded-xl text-sm"
        >
            <ModifyIcon />
        </Link>
    )
  }
'use client'

import BackToIcon from "@/_icons/back-to";
import Link from "next/link";
import React from "react";

interface Props {
    backTo: string,
}

export default function BackButton({ backTo }: Props) {
    return (
        <Link href={backTo}>
            <div className="btn back-btn flex items-center space-between">
                <BackToIcon />
                <p className="text-sm">Back</p>
            </div>
        </Link>
    )
  }
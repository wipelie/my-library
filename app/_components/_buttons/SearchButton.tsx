'use client'

import SearchIcon from "@/_icons/search"
import { ChangeEventHandler } from "react"

interface Props {
    type?: string,
    onSearch?: ChangeEventHandler,
}

export default function SearchButton({ type, onSearch }: Props) {
    return (
        <div className="search-container flex border-2 border-gray-light rounded-xl focus-within:border-gray m-2">
            <input
                type="search" id={`${type}-search`}
                className="rounded-xl font-mono border-none focus:outline-none focus:border-none" 
                placeholder="Search..."
                onChange={onSearch}
            />
            <span className="flex m-3 items-center"><SearchIcon /></span>
        </div>  
    )
  }
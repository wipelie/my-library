'use client'

import RemoveIcon from "@/_icons/remove"
import { Button } from "@chakra-ui/react"
import { MouseEventHandler } from "react"

interface Props {
    type?: string,
    onRemove?: MouseEventHandler,
}

export default function RemoveButton({ type, onRemove }: Props) {
    return (
        <button
            className="btn remove-btn flex items-center rounded-xl text-sm"
            onClick={onRemove}
        >
            <RemoveIcon />
        </button>
    )
  }
'use client'

import AddIcon from "@/_icons/add";
import Link from "next/link";
import React from "react";

interface Props {
    type?: string
}

export default function AddButton({ type }: Props) {
    return (
        <Link 
            href={`/${type}/new`}
            className="btn add-btn m-2"
        >
            <AddIcon />
        </Link>
    )
  }
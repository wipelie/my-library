interface Props {
    loading: boolean,
}

export function Loading({ loading } : Props) {
    const display = loading ? "inherit" : "hidden";

    return (
        <span className={`${display} inset-y-1/2 inset-x-1/2 loader fixed`}></span>
    );
}


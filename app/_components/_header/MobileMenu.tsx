'use client'

import { useState } from "react"
import HeaderList from "./HeaderList"

export default function MobileMenu() {
    const [isOpen, setisOpen] = useState(false)

    return (
        <div className="w-full block mr-5">
            <button
                className="flex items-center px-3 py-2 border rounded text-black border-black hover:text-amber-700 hover:border-amber-700"
                onClick={() => setisOpen(!isOpen)}
            >
                <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
            </button>
            {isOpen &&
                <HeaderList />
            }
        </div>
    )
  }
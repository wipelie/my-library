import { GetImageFromBucket } from "@/_utils/get-images"
import Image from "next/image"
import Link from "next/link"

export default function HeaderProfile() {
    const urlImg = GetImageFromBucket("avatars", "user.png")

    return (
        <Link href="/profile">
            <div className="header-profile-container profile">
                <Image
                    src={urlImg}
                    width={350}
                    height={350}
                    alt={"avatar"}
                />
            </div>
        </Link>
    )
  }
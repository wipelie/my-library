'use client'

import HeaderList from "./HeaderList"

export default function DesktopMenu() {
    return (
        <div className="flex-grow flex items-center w-auto">
            <HeaderList />
        </div>
    )
  }
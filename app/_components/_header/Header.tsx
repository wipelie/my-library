'use client'

import Link from "next/link"

import DesktopMenu from "./DesktopMenu"
import MobileMenu from "./MobileMenu"
import { useEffect, useState } from "react"
import { useAuth } from "@/providers/authProvider"

export default function Header() {
    const [isClient, setIsClient] = useState(false)

    const auth = useAuth()

    useEffect(() => {
        setIsClient(true)
    }, [])

    console.log(isClient, "hello", auth.token)

    return (
        <nav className="flex lg:items-center lg:justify-between flex-wrap bg-slate-100 p-6">
            { (isClient && auth.token) ?
                <>
                    <div className="block lg:hidden">
                        <MobileMenu />
                    </div>
                    <div className="flex lg:items-center flex-shrink-0 text-black mr-6">
                        <Link 
                            className="font-semibold text-xl tracking-tight"
                            href={"/dashboard"}
                        >My libraries</Link>
                    </div>
                    <div className="hidden lg:inline-block">
                        <DesktopMenu />
                    </div>
                </>
            :
                <div className="flex lg:items-center flex-shrink-0 text-black mr-6">
                    <Link 
                        className="font-semibold text-xl tracking-tight"
                        href={"/login"}
                    >My libraries</Link>
                </div>
            }

        </nav>
    )
  }
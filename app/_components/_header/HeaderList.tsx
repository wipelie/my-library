'use client'

import Link from "next/link"
import HeaderProfile from "./HeaderProfile"

const listsNames = ['Consoles', 'Games', 'Mangas', 'BDs']

export default function HeaderList() {

    return (
        <div className="text-sm lg:flex-grow flex items-center">
            {
                listsNames.map(name => 
                    <Link 
                        key={`header-list-${name}`}
                        href={`/${name.toLowerCase()}`}
                        className="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-grey mr-4">
                        {name}
                    </Link>
                )
            }
            <HeaderProfile />
        </div>
    )
  }
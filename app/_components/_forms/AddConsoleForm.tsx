import { hasName } from "@/_utils/checks";
import React, { FormEvent, useState } from "react";
import slugify from "react-slugify";

interface Props {
    addMutation: any,
}

interface SubmitProps {
    isPending: boolean,
}  

function Submit({ isPending }: SubmitProps) {
    return (
        <button 
            className="btn add-btn" 
            disabled={isPending}>
            { isPending ? "On the road..." : "Submit" }
        </button>

    )
}
  
export default function AddConsoleForm({ addMutation }: Props) {
    const [file, setFile] = useState({});

    const onSubmit =  (event: HTMLFormElement) => {
        event.preventDefault();
        addMutation.reset()

        const formData = new FormData(event.currentTarget);
        const name = formData.get('name') as string;

        if (!hasName(name)) {
            return;
        }

        const description = formData.get('description') as string;
        const slug = slugify(name);
        const hasFile: boolean = file instanceof File;

        const fileExt = hasFile ? file.name?.split('.').pop() : null;
        const fileName = hasFile ? `${slug}.${fileExt}` : null;

        const data = {
            slug,
            name,
            file,
            fileName,
            description,
        };

        addMutation.mutate(data);
    }

    const onChangedImg = (event: FormEvent<HTMLFormElement>) => {
        setFile(event.currentTarget.files[0]);
    }

    return (
        <form onSubmit={onSubmit} className="flex flex-col items-center m-10">
            <div className="my-5 w-1/2">
                <label className="block text-sm font-medium leading-6 text-gray-900">Name</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <input type="text" maxLength={25} name="name" id="name" className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-pink-500 sm:text-sm sm:leading-6" placeholder="Super Nintendo" />
                </div>
            </div>
            <div className="my-5 w-1/2">
                    <label className="block text-sm font-medium leading-6 text-gray-900">Details</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <span className="text-gray-500 sm:text-sm"></span>
                    </div>
                    <textarea name="description" id="description" rows={5} maxLength={300} className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-pink-500 sm:text-sm sm:leading-6" placeholder="Super Nintendo" />
                </div>
            </div>
            <div className="my-5">
                    <label className="block text-sm font-medium leading-6 text-gray-900">Upload your image !</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <span className="text-gray-500 sm:text-sm"></span>
                    </div>
                    <input
                        type="file" name="file" id="file"
                        onChange={onChangedImg}
                        className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-pink-500 sm:text-sm sm:leading-6" placeholder="super-nintendo.jpg" />
                </div>
            </div>
            <Submit isPending={addMutation.isPending} />
        </form>
    )}

import React, { useState } from "react";

interface Props {
    data: DataProps,
    slug: string,
    updateMutation: any,
}

interface DataProps {
    name: string,
    description: string,
}

interface SubmitProps {
    isPending: boolean,
}  


function Submit({ isPending }: SubmitProps) {
    return (
        <button 
            className="btn add-btn" 
            disabled={isPending}>
            { isPending ? "On the road..." : "Submit" }
        </button>

    )
}
  
export default function UpdateConsoleForm({ data, slug, updateMutation }: Props) {
    const [file, setFile] = useState({});

    const onSubmit =  (event: HTMLFormElement) => {
        event.preventDefault();
        updateMutation.reset()

        const formData = new FormData(event.currentTarget);

        const description = formData.get('description') as string;

        // const hasFile: boolean = file instanceof File;

        // const fileExt = hasFile ? file.name?.split('.').pop() : null;
        // const fileName = hasFile ? `${slug}.${fileExt}` : null;

        const updateData = {
            ...data,
            description: description
        };

        updateMutation.mutate(updateData);
    }

    const onChangedImg = (event: HTMLFormElement) => {
        setFile(event.currentTarget.files[0]);
    }

    return (
        <form onSubmit={onSubmit} className="flex flex-col items-center m-10">
            <div className="my-5">
                <label className="block text-sm font-medium leading-6 text-gray-900">Name</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <p id="name" className="block w-full text-gray-900">
                        {data.name}
                    </p>
                </div>
            </div>
            <div className="my-5">
                    <label className="block text-sm font-medium leading-6 text-gray-900">Details</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <span className="text-gray-500 sm:text-sm"></span>
                    </div>
                    <textarea
                        name="description" id="description"
                        rows={10} maxLength={500}
                        className="block rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-pink-500 sm:text-sm sm:leading-6"
                        defaultValue={data?.description} />
                </div>
            </div>
            {/* <div className="my-5">
                    <label className="block text-sm font-medium leading-6 text-gray-900">Upload your image !</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <span className="text-gray-500 sm:text-sm"></span>
                    </div>
                    <input
                        type="file" name="slug" id="slug"
                        onChange={onChangedImg}
                        className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-pink-500 sm:text-sm sm:leading-6" placeholder="super-nintendo.jpg" />
                </div>
            </div> */}
            <Submit isPending={updateMutation.isPending} />
        </form>
    )}

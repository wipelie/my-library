'use client'

import { brands } from "@/_utils/constants";
import React, { useEffect, useState } from "react";

interface Props {
    type?: string
    onBrandChange: Function
}

export default function BrandFilter({ onBrandChange }: Props) {
    const [brand, setBrand] = useState(undefined)

    useEffect(() => {
        onBrandChange({ name: "brand", value: brand })
    }, [brand])
    
    return (
        <div className="select-brand-container m-2">
            <select
                name="select-brand"
                id="select-brand"
                className="h-full flex border-2 border-gray-light rounded-xl focus-within:border-gray"
                onChange={(event) => setBrand(event.target?.value)}
            >
                { brands.map(brand =>
                    <option key={`brand-${brand.key}`} value={brand.key}>{brand.value}</option>
                )}
            </select>
        </div>

    )
  }
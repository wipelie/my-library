'use client'

import { ChangeEventHandler } from "react";
import AddButton from "@/_components/_buttons/AddButton"
import SearchButton from "../_buttons/SearchButton";
import BrandFilter from "../_select/BrandFilter";

interface Props {
    onSearch?: ChangeEventHandler,
    // onNew?: MouseEventHandler,
    onBrandChange: ChangeEventHandler
}

export default function DashboardActions({ onSearch, onBrandChange }: Props) {
    return (
        <div className="dashboard-actions-container flex justify-end items-center mb-5">
            <div className="flex">
                <AddButton type={"consoles"} />
                <BrandFilter onBrandChange={onBrandChange} />
                <SearchButton onSearch={onSearch} />
            </div>
        </div>
    )
  }
'use client'

import { MouseEventHandler } from "react";
import BackButton from "../_buttons/BackButton";
import RemoveButton from "../_buttons/RemoveButton";
import ModifyButton from "../_buttons/ModifyButton";

interface Props {
    backTo?: string,
    uuid?: string,
    canModify?: boolean,
    canRemove?: boolean,
    onRemove?: MouseEventHandler,
    type?: string
}

export default function DetailsActions({ type = "", uuid = "", backTo, canRemove = true, canModify = true, onRemove}: Props) {
    return (
        <div className="details-actions-container flex justify-between items-center mb-5">
            {backTo && <BackButton backTo={backTo} />}
            <div className="flex">
                {canModify && <ModifyButton type={type} uuid={uuid} /> }
                {canRemove && <RemoveButton onRemove={onRemove} /> }
            </div>
        </div>
    )
  }
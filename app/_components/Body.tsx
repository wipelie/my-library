import { ReactNode } from "react";
import { Inter } from 'next/font/google'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

import ToastProvider from "@/_utils/toast.provider";
import Header from "./_header/Header";
import Footer from "./Footer";
import ModalProvider from "@/providers/modalProvider";
import { ChakraProvider } from "@chakra-ui/react";
import AuthProvider from "@/providers/authProvider";

interface LayoutProps {
    children: ReactNode;
}
  
const inter = Inter({ subsets: ['latin'] })

export default function Body({ children }: LayoutProps) {
    return (
        <body className={inter.className}>
            <ChakraProvider>
                <AuthProvider>
                    <ModalProvider>
                        <ToastProvider>
                            <Header />
                            <main>
                                {children}
                            </main>
                            <Footer />
                        </ToastProvider>
                    </ModalProvider>
                </AuthProvider>
            </ChakraProvider>
            <ReactQueryDevtools />
        </body>
    )
}
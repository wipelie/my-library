'use client'

import { colorVariants } from "@/_utils/constants";
import Link from "next/link";
import { useMemo } from "react";

interface Props {
    urlImg: string;
    goTo: string;
    data: DataProps;
}

interface DataProps {
    img: string;
    name: string;
    slug: string;
}
  

function Card({ urlImg, goTo, data }: Props) {
    const randomColor: string = useMemo(() => {
        const randomNumber = Math.floor(Math.random() * 10);
        const colors = ["yellow", "yellowdark", "blue", "bluedark", "redlight",  "red", "reddark", "green", "greendark", "black"]

        return colors[randomNumber]
    }, []);

    const color = colorVariants[randomColor]
    
    return (
        <Link 
            className={`card h-48
            is-dark min-h-s m-5 min-w-20
          bg-white border-double border-4
            flex flex-col align-center ${color}  ease-in`}
            href={goTo}
        >
            { urlImg &&
                <div
                    after-dynamic-value={data.name}
                    className={`h-full card-container
                        after:content-[attr(after-dynamic-value)] bg-contain bg-center bg-no-repeat`}
                    style={{ backgroundImage: `url(${urlImg})`}}
                ></div>
            }
        </Link>
    );
};

export default Card;
'use client'

export default function Footer() {
    const listsNames = ['Consoles', 'Games', 'Mangas', 'BDs']

    return (
        <footer className="flex items-end justify-end bg-slate-100 p-6">
            <span className="text-sm tracking-tight">Copyright</span>
        </footer>
    )
  }